import PackageDescription

let package = Package(
    name: "TesseractOCRMac",
    dependencies: [
        .Package(url: "https://jianhaos@bitbucket.org/jianhaos/ctesseract.git", majorVersion: 1),
        // For mac os debugging
//        .Package(url: "https://jianhaos@bitbucket.org/jianhaos/cpng.git", majorVersion: 1),
        // For deployment
        .Package(url: "https://github.com/s1ddok/Cpng.git", majorVersion: 1)
    ]
)

