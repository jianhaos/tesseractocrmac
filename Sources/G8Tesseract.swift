//
//  G8Tesseract.swift
//  PerfectTemplate
//
//  Created by Jianhao Song on 3/5/17.
//
//

import Foundation
import cTesseract
import Cpng

public class G8Tesseract: NSObject {

    private var _charWhitelist: String = ""
    public var charWhitelist: String {
        set {
            if _charWhitelist != newValue {
                _charWhitelist = newValue
                self.setVariableValue(_charWhitelist, forKey: kG8ParamTesseditCharWhitelist)
            }
        }
        get { return _charWhitelist }
    }
    
    private var _charBlacklist: String = ""
    public var charBlacklist: String {
        set {
            if _charBlacklist != newValue {
                _charBlacklist = newValue
                self.setVariableValue(_charBlacklist, forKey: kG8ParamTesseditCharBlacklist)
            }
        }
        get { return _charBlacklist }
    }
    
    private var _imageData: Data!
    public var imageData: Data! {
        set {
            if _imageData != newValue {
                if let pix = getPix(for: newValue) {
                    TessBaseAPISetImage(tesseract, pix.data, pix.width, pix.height, pix.rowBytes / pix.width, pix.rowBytes)
                }
    
                _imageData = newValue
            }
        }
        get {
            return _imageData
        }
    }

    let kG8ParamTesseditCharWhitelist: String = "tessedit_char_whitelist"
    let kG8ParamTesseditCharBlacklist: String = "tessedit_char_blacklist"
    
    var variables: [String: String]!
    var tesseract: OpaquePointer!
    

    public func clear() {
        TessBaseAPIClear(tesseract)
    }
    
    public override init() {
        super.init()
        
        variables = [String: String]()
        tesseract = TessBaseAPICreate()
        configEngine()
    }
    
    deinit {
        if tesseract != nil {
            TessBaseAPIDelete(tesseract)
            tesseract = nil
        }
    }
    
    func configEngine() {
        TessBaseAPIInit3(tesseract, nil, nil)
    }
    
    func setVariableValue(_ value: String, forKey key: String) {
        variables[key] = value
        TessBaseAPISetVariable(tesseract, NSString(string: key).utf8String, NSString(string: value).utf8String)
    }
    
    func variableValue(forKey key: String) -> String? {
        return String(utf8String: TessBaseAPIGetStringVariable(tesseract, NSString(string: key).utf8String))
    }
    
    func setVariablesFromDictionary(_ dictionary: [String: String]) {
        for key in dictionary.keys {
            if let value = dictionary[key] {
                setVariableValue(value, forKey: key)
            }
        }
    }
    
    func loadVariables() {
        for key in variables.keys {
            if let value = variables[key] {
                TessBaseAPISetVariable(tesseract, NSString(string: key).utf8String, NSString(string: value).utf8String)
            }
        }
    }
    
    
    // MARK: - Result fetching
    
    public func recognizedText() -> String? {
        guard let utf8Text = TessBaseAPIGetUTF8Text(tesseract) else {
            print("No recognized text. Check that -[Tesseract setImage:] is passed an image bigger than 0x0.")
            return nil
        }
        return String(utf8String: utf8Text)
    }
    
    
    // MARK: - Other functions
    
    func getPix(for imageData: Data) -> (data: [UInt8], width: Int32, height: Int32, rowBytes: Int32)? {
        // Init png and info struct
        let png = png_create_read_struct(PNG_LIBPNG_VER_STRING, nil, nil, nil)
        let info = png_create_info_struct(png)
        
        // Read image from file
        let directory = NSTemporaryDirectory()
        let fileName = "tesseract_" + NSUUID().uuidString
        
        // This returns a URL? even though it is an NSURL class method
        guard let fullURL = NSURL.fileURL(withPathComponents: [directory, fileName]) else { return nil }
        try? imageData.write(to: fullURL, options: .atomic)
        let fp = fopen(fullURL.path, "r")
        png_init_io(png, fp)
        png_read_info(png, info)
        
        // Get image info
        let width = png_get_image_width(png, info)
        let height = png_get_image_height(png, info)
        let rowBytes = png_get_rowbytes(png, info)
        
        // Read image to memory
        let row_pointers = png_bytepp.allocate(capacity: MemoryLayout<png_bytep>.size * Int(height))
        for y in 0..<Int(height) {
            row_pointers[y] = png_bytep.allocate(capacity: rowBytes)
        }
        png_read_image(png, row_pointers)
        
        //                let imgDataProvider = CGDataProvider(data: newValue as CFData)
        //                let cgImage = CGImage(pngDataProviderSource: imgDataProvider!, decode: nil, shouldInterpolate: false /* Need to be false here */, intent: CGColorRenderingIntent.defaultIntent)
        //                let imgData = cgImage?.dataProvider?.data
        
        // Get pixel data
        var data = [UInt8]()
        for y in 0..<Int(height) {
            for x in 0..<Int(rowBytes) {
                data.append(row_pointers[y]![x])
            }
        }
        
        // Close file
        fclose(fp)
        
        return (data, Int32(width), Int32(height), Int32(rowBytes))
    }
    
    public func recognize() {
        TessBaseAPIRecognize(tesseract, nil)
    }
    
}
